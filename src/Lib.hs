{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( someFunc
    ) where

import           Control.Applicative
import qualified Data.Text as T
import           Data.String
import Data.Maybe
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import Text.Read (Lexeme(Number))
import qualified GHC.Base as T

data TestField = TestField Int Int String String deriving (Show)

data FieldInsert = FieldInsert Int String String deriving (Show)

data FieldSelect = FieldSelect (Maybe Int) (Maybe Int) (Maybe String) (Maybe String)

instance FromRow TestField where
  fromRow = TestField <$> field <*> field <*> field <*> field

instance ToRow TestField where
  toRow (TestField id phone mail name) = toRow (id, phone, mail, name)

instance ToRow FieldInsert where
  toRow (FieldInsert phone mail name) = toRow (phone, mail, name)

instance ToRow FieldSelect where
  toRow (FieldSelect id phone mail name) = toRow (id, phone, mail, name)

-- conn::IO Connection
-- conn = open "test.db"

insertRow :: FieldInsert -> IO ()
insertRow row = do
  conn <- open "test.db"
  str <- readFile "queries/insert.txt"
  putStrLn str
  execute conn (fromString str) row
  close conn

-- selectRow :: TestField -> IO([FieldInsert])
-- selectRow row = 

mainDict::[FieldInsert]
mainDict = []

someFunc :: IO ()
someFunc = do
  conn <- open "test.db"
  --str <- readFile "tmp.txt"
  insertRow (FieldInsert 5 "fkfk" "fjfjfj")
  execute_ conn "DELETE FROM contactsID WHERE id = 1"
  resp <- query conn "SELECT * FROM contactsID WHERE id = ? AND phone = ? AND email = ? AND name = ?" (FieldSelect Nothing Nothing (Just "4") Nothing) :: IO[TestField]
  --query conn "select ? + ?" (40 :: Double, 2 :: Double)
  executeNamed conn "UPDATE contactsID SET phone = :phone WHERE id = :id" [":id" := (13 :: T.Int), ":phone" := (8800::T.Int)]
  putStrLn (show resp)
  putStrLn "Good!"
  --close conn
